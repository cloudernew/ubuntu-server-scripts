#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Create a regular user...'
read -p 'Username: ' username
adduser $username
chmod o-rw /home/$username
