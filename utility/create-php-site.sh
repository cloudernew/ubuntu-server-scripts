#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

scriptDir=`pwd`/`dirname "$0"`

echo 'Creating PHP Site...'
read -p 'Username of site owner: ' username
read -p 'Site name (example.com): ' sitename

# create user directory structure
sudo -u $username mkdir -p /home/$username/www/$sitename/public_html
mkdir /home/$username/www/$sitename/logs
chmod go-w /home/$username/www/$sitename/logs

# create vhost file for site
cp $scriptDir/templates/vhost-php /etc/apache2/sites-available/$sitename.conf
sed -i -e s/{{username}}/$username/g \
       -e s/{{sitename}}/$sitename/g /etc/apache2/sites-available/$sitename.conf

# enable site and restart server
a2ensite $sitename
service apache2 restart
