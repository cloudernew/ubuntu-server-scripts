#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

scriptDir=`pwd`/`dirname "$0"`

echo 'Creating new MySQL user...'
read -p 'Username: ' username
read -p 'Password (for new user): ' password
echo
echo 'MySQL root user prompt...'

# fill in the template and pipe it out to mysql
sed -e s/{{username}}/$username/g \
    -e s/{{password}}/$password/g $scriptDir/templates/mysql-create-user |
mysql -u root -p
