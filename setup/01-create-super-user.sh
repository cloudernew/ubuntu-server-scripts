#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Creating a superuser...'
read -p 'Username: ' username
adduser $username
# # don't allow others to list or write to user's home directory
chmod o-rw /home/$username
# # give the new user root access
addgroup $username sudo
