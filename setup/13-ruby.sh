#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

scriptDir=`pwd`/`dirname "$0"`

echo 'Installing Ruby...'

rubyurl='http://cache.ruby-lang.org/pub/ruby/2.1/ruby-2.1.0.tar.gz'
rubyversion=`basename $rubyurl | sed s/.tar.gz//g`

mkdir $scriptDir/downloads
cd $scriptDir/downloads
wget $rubyurl

# Get dependencies
aptitude update
deps="build-essential openssl libreadline6 libreadline6-dev curl zlib1g "
deps="$deps zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 "
deps="$deps libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake "
deps="$deps libtool bison nodejs"
aptitude install $deps

tar xvfz `basename $rubyurl`
cd $rubyversion
./configure
make
make install

# update RubyGems
gem update --system --no-user-install
gem update --no-user-install --no-document
