#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Installing MySQL...'
aptitude update
aptitude install mysql-server
# post install configuration helper scripts
mysql_install_db
mysql_secure_installation
