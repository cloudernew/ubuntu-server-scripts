#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

scriptDir=`pwd`/`dirname "$0"`

echo 'Installing Phusion Passenger...'

# Get dependencies
deps="libcurl4-openssl-dev apache2-threaded-dev libapr1-dev libaprutil1-dev"
aptitude update
aptitude install $deps

# Install Passenger for Apache2
gem install passenger --no-user-install --no-document
passenger-install-apache2-module

# Configure Apache2 to use Passenger
passengerRoot=`passenger-config --root`
passengerModule="$passengerRoot/libout/apache2/mod_passenger.so"
passengerConfig="/etc/apache2/conf-available/passenger.local.conf"
defaultRuby=`which ruby`
echo "LoadModule passenger_module $passengerModule" > $passengerConfig
echo "PassengerRoot $passengerRoot" >> $passengerConfig
echo "PassengerDefaultRuby $defaultRuby" >> $passengerConfig

service apache2 restart


