#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Configuring SSH server...'
sed -i -e 's/^\(PermitRootLogin\) .\+/\1 without-password/g' \
       -e '/^UseDNS \(no\|yes\)/d' /etc/ssh/sshd_config
echo 'UseDNS no' >> /etc/ssh/sshd_config

reload ssh
