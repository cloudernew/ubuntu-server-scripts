#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Installing Fail2Ban...'
aptitude update
aptitude install fail2ban
cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
service fail2ban restart
