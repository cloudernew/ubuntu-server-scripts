#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Installing Apache2...'
aptitude update
aptitude install apache2

# don't let others list sites-(available|enabled)
chmod o-r /etc/apache2/sites-*

# Configure Apache2
sed -i -e 's/^\(ServerTokens\) .\+/\1 Prod/g'
       -e 's/^\(ServerSignature\) .\+/\1 Off/g' /etc/apache2/conf.d/security
# # disable all modules initially
rm /etc/apache2/mods-enabled/*
# # reenable only the modules that are absolutely needed
a2enmod authz_host dir mime rewrite

service apache2 restart
