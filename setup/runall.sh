#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

./00-first-login.sh
./01-create-super-user.sh
./02-sshd-config.sh
./03-firewall.sh
./04-fail2ban.sh
./10-apache.sh
