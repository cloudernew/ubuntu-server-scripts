#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Installing PHP5...'
aptitude update
extensions="php5-mcrypt php5-mysql php5-curl php5-gd"
aptitude install php5 libapache2-mod-php5 $extensions

# Configure PHP5
# # this next line might not be needed after installing mod_php5
# # sed -i --follow-symlinks 's/DirectoryIndex \(index.php \)\?/DirectoryIndex index.php /g' /etc/apache2/mods-enabled/dir.conf
# # disable short_open_tags
sed -i -e 's/^\(short_open_tag =\).\+/\1 Off/g' \
       -e 's/^\(expose_php =\) .\+/\1 Off/g' /etc/php5/apache2/php.ini

