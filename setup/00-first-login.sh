#!/bin/bash
if [ "$(id -u)" != "0" ]; then
  echo "You need root access to run this script." 2>&1
  echo "Try running again with 'sudo'." 2>&1
  exit 1
fi

echo 'Change the default password'
passwd

# Don't allow listing of home dir
chmod o-r /home

# Setup /etc/skel
# # create www dir and don't allow listing by others
mkdir /etc/skel/www
chmod o-r /etc/skel/www
# # create .ssh dir and restrict permissions to user only
mkdir /etc/skel/.ssh
chmod go-rwx /etc/skel/.ssh
# # create authorized_keys files with a public key for a master private key
pubkey="ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAIBzXCdhI8UhyZhNqqLa4XzldMhJOrxNpA8A8CZJ9XNxJIEsn5PcGt4OGzYKd7KlJffCSsccyFjTIDYVVhLCxOdJeb6FP+PpmK2+PiYfasGoWUwlIY7M9aQMOeCTqEhikKyyjkIFhAaDLzp8Y8KQB57YDOR9kuAzUuCBBNGkVj2uWQ=="
echo $pubkey > /etc/skel/.ssh/authorized_keys
# # create .gemrc so users can install gems into home directory
echo "gem: --no-document --user-install" > /etc/skel/.gemrc
# # include executables from user install gems in PATH
echo "" >> /etc/skel/.profile
echo "# set PATH so it includes users's gem bin if it exists" >> /etc/skel/.profile
echo "if [ -d \"\$HOME/.gem/ruby/2.0.0/bin\" ] ; then" >> /etc/skel/.profile
echo "    PATH=\"\$HOME/.gem/ruby/2.0.0/bin:\$PATH\"" >> /etc/skel/.profile
echo "fi" >> /etc/skel/.profile
